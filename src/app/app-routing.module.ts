import { LeadingComment } from '@angular/compiler';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccountsViewComponent } from './accounts-view/accounts-view.component';
import { DematAccountsComponent } from './demat-accounts/demat-accounts.component';
import { LandingLeadsComponent } from './landing-leads/landing-leads.component';
import { LoginComponent } from './login/login.component';
import { WebsiteLeadsComponent } from './website-leads/website-leads.component';

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full'  },
  { path: 'login', component: LoginComponent },
  { path: 'landing', component: LandingLeadsComponent },
  { path: 'website', component: WebsiteLeadsComponent },
  { path: 'demat', component: DematAccountsComponent },
  { path: 'view', component: AccountsViewComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }