import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LandingLeadsComponent } from './landing-leads/landing-leads.component';
import { WebsiteLeadsComponent } from './website-leads/website-leads.component';
import { DematAccountsComponent } from './demat-accounts/demat-accounts.component';
import { LoginComponent } from './login/login.component';
import { AccountsViewComponent } from './accounts-view/accounts-view.component';

@NgModule({
  declarations: [
    AppComponent,
    LandingLeadsComponent,
    WebsiteLeadsComponent,
    DematAccountsComponent,
    LoginComponent,
    AccountsViewComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
