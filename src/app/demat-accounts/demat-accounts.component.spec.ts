import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DematAccountsComponent } from './demat-accounts.component';

describe('DematAccountsComponent', () => {
  let component: DematAccountsComponent;
  let fixture: ComponentFixture<DematAccountsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DematAccountsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DematAccountsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
