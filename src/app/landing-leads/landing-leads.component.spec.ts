import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LandingLeadsComponent } from './landing-leads.component';

describe('LandingLeadsComponent', () => {
  let component: LandingLeadsComponent;
  let fixture: ComponentFixture<LandingLeadsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LandingLeadsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LandingLeadsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
